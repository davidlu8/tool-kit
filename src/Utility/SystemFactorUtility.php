<?php

namespace ToolKit\Utility;

class SystemFactorUtility
{
    /**
     * 获取当前时间戳
     * @return int
     */
    public function getTimestamp()
    {
        return time();
    }

    /**
     * 获取当前时间微秒
     * @return false|string
     */
    public function getMicroSecond()
    {
        $data = explode(" ", microtime());
        return substr($data[0], 2, 6);
    }

    /**
     * 获取唯一键ID
     * @return string
     */
    public function getUniqueId()
    {
        return md5(uniqid(mt_rand(), true));
    }
}