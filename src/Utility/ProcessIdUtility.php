<?php

namespace ToolKit\Utility;

class ProcessIdUtility
{
    /** @var SystemFactorUtility $systemFactor */
    protected $systemFactor;

    public function __construct(SystemFactorUtility $systemFactor)
    {
        $this->systemFactor = $systemFactor;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $uniqueId = $this->getUniqueId();
        return sprintf('%s-%s-%s-%s-%s', $this->getDate(), $this->getTime(), $this->systemFactor->getMicroSecond(), substr($uniqueId, 0, 4), substr($uniqueId, 4, 8));
    }

    /**
     * @return false|string
     */
    protected function getDate()
    {
        return date("Ymd", $this->systemFactor->getTimestamp());
    }

    /**
     * @return false|string
     */
    protected function getTime()
    {
        return date('His', $this->systemFactor->getTimestamp());
    }

    /**
     * @return string
     */
    protected function getUniqueId()
    {
        return $this->systemFactor->getUniqueId();
    }
}