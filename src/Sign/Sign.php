<?php

namespace ToolKit\Sign;

use Illuminate\Support\Facades\Facade;

/**
 * Class Sign
 * @package ToolKit\Robot
 *
 * @method static string setSignName(string $signName)
 * @method static string setExcludeKeys(array $excludeKeys)
 * @method static string generate(array $params, string $secret)
 * @method static bool verify(array $params, string $secret)
 * @method static array attach(array $params, string $secret)
 * @method static array getDebugInfo()
 *
 * @see SignManager
 */
class Sign extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Sign';
    }
}