<?php

namespace ToolKit\Sign;

use Illuminate\Support\ServiceProvider;
use ToolKit\Sign\Engine\AlgorithmEngineInterface;

class SignServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Sign', function () {
            return new SignManager($this->app->make(AlgorithmEngineInterface::class));
        });
    }
}
