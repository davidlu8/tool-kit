<?php

namespace ToolKit\DataContainer\Engine;

class ProcessEngine implements StorageEngineInterface
{
    /** @var array $storage */
    protected $storage;

    /**
     * @param $namespace
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($namespace, $key, $default = null)
    {
        $value = $default;
        if (array_key_exists($namespace, $this->storage) && array_key_exists($key, $this->storage[$namespace])) {
            $value = $this->storage[$namespace][$key];
        }
        return $value;
    }

    /**
     * @param $namespace
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($namespace, $key, $value)
    {
        $this->storage[$namespace][$key] = $value;
        return true;
    }

}