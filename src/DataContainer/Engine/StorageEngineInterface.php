<?php

namespace ToolKit\DataContainer\Engine;

interface StorageEngineInterface
{
    /**
     * @param $namespace
     * @param $key
     * @param mixed $default
     * @return mixed
     */
    public function get($namespace, $key, $default = null);

    /**
     * @param $namespace
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($namespace, $key, $value);
}