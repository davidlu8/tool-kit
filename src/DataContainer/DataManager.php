<?php

namespace ToolKit\DataContainer;

use ToolKit\DataContainer\Engine\StorageEngineInterface;

class DataManager
{
    /** @var DataConfigService $dataConfigService */
    protected $dataConfigService;
    /** @var StorageEngineInterface $storageEngine */
    protected $storageEngine;

    public function __construct(DataConfigService $dataConfigService, StorageEngineInterface $storageEngine)
    {
        $this->dataConfigService = $dataConfigService;
        $this->storageEngine = $storageEngine;
        $this->initialize();
    }

    /**
     * User: Luw
     * Datetime: 2020/7/28 17:01
     * @return bool
     */
    public function initialize()
    {
        $initializeData = $this->dataConfigService->initializeData();
        if (!empty($initializeData)) {
            foreach ($initializeData as $item) {
                $this->set($item['namespace'], $item['key'], $item['value']);
            }
        }
        return true;
    }

    /**
     * @param $namespace
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($namespace, $key, $default = null)
    {
        return $this->storageEngine->get($namespace, $key, $default);
    }

    /**
     * @param $namespace
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($namespace, $key, $value)
    {
        return $this->storageEngine->set($namespace, $key, $value);
    }
}