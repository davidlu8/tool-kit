<?php

namespace ToolKit\DataContainer;

use Illuminate\Support\ServiceProvider;
use ToolKit\DataContainer\Engine\StorageEngineInterface;

class DataServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('DataContainer', function () {
            return new DataManager($this->app->make(DataConfigService::class), $this->app->make(StorageEngineInterface::class));
        });
    }
}