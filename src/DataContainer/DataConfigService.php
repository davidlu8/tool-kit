<?php

namespace ToolKit\DataContainer;

use ToolKit\Utility\ProcessIdUtility;

class DataConfigService
{
    const NAMESPACE_GLOBAL = 'global';

    /** @var ProcessIdUtility $processIdUtility */
    protected $processIdUtility;

    public function __construct(ProcessIdUtility $processIdUtility)
    {
        $this->processIdUtility = $processIdUtility;
    }

    /**
     * @return array
     */
    public function initializeData()
    {
        return [
            [
                'namespace' => static::NAMESPACE_GLOBAL,
                'key' => 'process_id',
                'value' => $this->processIdUtility->generate()
            ]
        ];
    }
}