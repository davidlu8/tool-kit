<?php

namespace ToolKit\DataContainer;

use Illuminate\Support\Facades\Facade;

/**
 * Class DataContainer
 * @package ToolKit\DataContainer
 *
 * @method static mixed get(string $namespace, string $key, $default)
 * @method static mixed set(string $namespace, string $key, $value)
 *
 * @see DataManager
 */
class DataContainer extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'DataContainer';
    }
}