<?php

namespace ToolKit\Robot;

use Illuminate\Support\Facades\Facade;

/**
 * Class Robot
 * @package ToolKit\Robot
 *
 * @method static mixed send(string $name, string $contentType, string $content)
 *
 * @see RobotManager
 */
class Robot extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Robot';
    }
}
