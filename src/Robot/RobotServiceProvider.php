<?php

namespace ToolKit\Robot;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use ToolKit\Robot\Engine\RobotEngineInterface;

class RobotServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Robot', function () {
            return new RobotManager($this->app->make(RobotEngineInterface::class), Config::get('robot.qywechat'));
        });
    }
}
