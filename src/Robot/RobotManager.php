<?php

namespace ToolKit\Robot;

use ToolKit\Exceptions\ValidationException;
use ToolKit\Robot\Engine\RobotEngineInterface;

class RobotManager
{
    /** @var RobotEngineInterface $robotEngine */
    protected $robotEngine;
    /** @var array $configData */
    protected $configData;

    public function __construct(RobotEngineInterface $robotEngine, array $configData)
    {
        $this->configData = $configData;
        $this->robotEngine = $robotEngine;
    }

    /**
     * 获取配置信息
     * @param $name
     * @param null $default
     * @return mixed|null
     */
    public function getConfig($name, $default = null)
    {
        $config = $default;
        if (isset($this->configData[$name]) && !empty($this->configData[$name])) {
            $config = $this->configData[$name];
        }
        return $config;
    }

    /**
     * @param $name
     * @param $contentType
     * @param $content
     * @return mixed
     * @throws ValidationException
     */
    public function send($name, $contentType, $content)
    {
        $config = $this->getConfig($name);
        if (empty($config)) {
            throw new ValidationException('config 不存在, robot:' . $robot);
        }
        return $this->robotEngine->send($config, $contentType, $content);
    }
}