<?php

namespace ToolKit\Robot\Engine;

interface RobotEngineInterface
{
    /**
     * 发送信息
     * @param $config
     * @param $contentType
     * @param $content
     * @return mixed
     */
    public function send($config, $contentType, $content);
}