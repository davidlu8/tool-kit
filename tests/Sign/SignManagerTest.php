<?php

namespace ToolKit\Tests\Sign;

use PHPUnit\Framework\MockObject\MockObject;
use ToolKit\Exceptions\ValidationException;
use ToolKit\Sign\Engine\FullParameterAlgorithmEngine;
use ToolKit\Sign\SignManager;
use ToolKit\Tests\TestCase;

class SignManagerTest extends TestCase
{
    /**
     * @return array
     */
    public function dpGenerateSceneExcludeKeysAssertTrue()
    {
        return [
            [
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                    'sign' => '4'
                ],
                [],
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                ]
            ],
            [
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                    'sign' => '4'
                ],
                ['a', 'b'],
                [
                    'c' => '3',
                ]
            ],
        ];
    }

    /**
     * @dataProvider dpGenerateSceneExcludeKeysAssertTrue
     * @param $params
     * @param $excludeKeys
     * @param $exceptedParams
     * @throws ValidationException
     */
    public function testGenerateSceneExcludeKeysAssertTrue($params, $excludeKeys, $exceptedParams)
    {
        /** @var MockObject|FullParameterAlgorithmEngine $algorithmEngineMock */
        $algorithmEngineMock = $this->getMockBuilder(FullParameterAlgorithmEngine::class)
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMock();
        $algorithmEngineMock->expects($this->once())
            ->method('generate')
            ->with($this->equalTo($exceptedParams));

        $signManager = new SignManager($algorithmEngineMock);
        $signManager->setExcludeKeys($excludeKeys);
        $signManager->generate($params, 'xxx');
    }


    /**
     * @throws ValidationException
     */
    public function testAttachSceneAttachAssertTrue() {
        /** @var FullParameterAlgorithmEngine|MockObject $algorithmEngineStub */
        $algorithmEngineStub = $this->getMockBuilder(FullParameterAlgorithmEngine::class)
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMock();
        $algorithmEngineStub->method('generate')
            ->willReturn('yyy');

        $signManager = new SignManager($algorithmEngineStub);
        $params = $signManager->attach([
            'a' => 1
        ], 'xxx');
        $this->assertTrue(isset($params['sign']));
        $this->assertTrue(isset($params['timestamp']));
        $this->assertTrue(isset($params['nonce_str']));
        $this->assertTrue($params['sign'] == 'yyy');
    }
}