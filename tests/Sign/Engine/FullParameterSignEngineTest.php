<?php

namespace ToolKit\Tests\Sign\Engine;

use stdClass;
use ToolKit\Sign\Engine\FullParameterAlgorithmEngine;
use ToolKit\Tests\TestCase;

class FullParameterSignEngineTest extends TestCase
{
    protected $fullParameterSignEngine;

    public function setUp()
    {
        $this->fullParameterSignEngine = new FullParameterAlgorithmEngine();
    }

    public function dpParseParams()
    {
        $emptyStd = new stdClass();
        $std = new stdClass();
        $std->d1 = 1;
        $std->d2 = [
            'v2', 'f' => [
                'f1'
            ]
        ];
        return [
            [
                [
                    'a' => '',
                    'b' => 1,
                    'c' => null,
                    'd' => true,
                    'e' => false,
                    'f' => [],
                    'g' => $emptyStd,
                ],
                [
                    'b' => 1,
                    'd' => 1,
                    'e' => 0,
                ]
            ],
            [
                [
                    'a' => 1,
                    'b' => 'ccc',
                    'c' => [
                        'c1' => 1,
                        'c2' => 2
                    ],
                    'd' => $std
                ],
                [
                    'a' => 1,
                    'b' => 'ccc',
                    'c.c1' => 1,
                    'c.c2' => 2,
                    'd.d1' => 1,
                    'd.d2.0' => 'v2',
                    'd.d2.f.0' => 'f1',
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpParseParams
     * @param $params
     * @param $parseParams
     */
    public function testParseParams($params, $parseParams)
    {
        $this->assertEquals($parseParams, $this->fullParameterSignEngine->parseParams($params));
    }

}